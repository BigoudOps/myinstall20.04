# myinstall20.04

just a script for a post install Ubuntu 20.04 in this repository you can find this software ⬇:

by category

💼 :

[Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Docker](https://docs.docker.com/get-started/overview/)

[Gdebi](https://launchpad.net/gdebi)

[Synaptic](http://www.nongnu.org/synaptic/)

[Virtualbox](https://www.virtualbox.org/)

🥂 :

[Mumble](https://www.mumble.com/)

[Discord](https://discord.com/)

🎮 :

[Lutris](https://lutris.net/) 

📺 :

[Vlc](https://www.videolan.org/) 

[kodi](https://kodi.tv/)

[Peek](https://github.com/phw/peek)

[Kazam](https://launchpad.net/kazam)

🛠 :

[gnome-tweaks](https://gitlab.gnome.org/GNOME/gnome-tweaks)

[gnome-sushi](https://en.wikipedia.org/wiki/GNOME_sushi)

[numix themes](https://numixproject.github.io/)

[PulseEffects](https://github.com/wwmm/pulseeffects)

[ShellCheck](https://github.com/koalaman/shellcheck)

🔐 :

[KeepassXC](https://keepassxc.org/)

⌨ :

[VSCodium](https://vscodium.com/)

[ADB and Fastboot](https://www.android.com/)